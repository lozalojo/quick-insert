import { customKeybindHandler } from "module/utils";
import { TinyMCEContext } from "./contexts";
import { QuickInsert } from "./core";

export function registerTinyMCEPlugin(): void {
  // TinyMCE addon registration
  tinymce.PluginManager.add("quickinsert", function (editor) {
    editor.on("keydown", (evt) => {
      const context = new TinyMCEContext(editor);
      customKeybindHandler(evt, context);
    });
    editor.ui.registry.addButton("quickinsert", {
      tooltip: "Quick Insert",
      icon: "search",
      onAction: function () {
        if (QuickInsert.app?.embeddedMode) return;
        // Open window
        QuickInsert.open(new TinyMCEContext(editor));
      },
    });
  });
  CONFIG.TinyMCE.plugins = CONFIG.TinyMCE.plugins + " quickinsert";
  CONFIG.TinyMCE.toolbar = CONFIG.TinyMCE.toolbar + " quickinsert";
}
